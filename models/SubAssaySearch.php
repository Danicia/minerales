<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubAssay;

/**
 * SubAssaySearch represents the model behind the search form about `app\models\SubAssay`.
 */
class SubAssaySearch extends SubAssay
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Quota-ID', 'Element', 'Calculation'], 'integer'],
            [['ProvisionalValue', 'FinalValue', 'Umpire', 'Payable', 'Escalator', 'AveragePrice', 'TotalPriced', 'MaterialPrice', 'TotalUnpriced', 'Penalty', 'TotalHedged', 'AveragePriceHedged', 'TreatmentCharge', 'RefiningCharge', 'TobeHedged', 'TobePriced', 'ContractualPayable', 'PricedQuantity'], 'number'],
            [['Measurement', 'Unit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubAssay::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Quota-ID' => $this->Quota-ID,
            'Element' => $this->Element,
            'ProvisionalValue' => $this->ProvisionalValue,
            'FinalValue' => $this->FinalValue,
            'Umpire' => $this->Umpire,
            'Calculation' => $this->Calculation,
            'Payable' => $this->Payable,
            'Escalator' => $this->Escalator,
            'AveragePrice' => $this->AveragePrice,
            'TotalPriced' => $this->TotalPriced,
            'MaterialPrice' => $this->MaterialPrice,
            'TotalUnpriced' => $this->TotalUnpriced,
            'Penalty' => $this->Penalty,
            'TotalHedged' => $this->TotalHedged,
            'AveragePriceHedged' => $this->AveragePriceHedged,
            'TreatmentCharge' => $this->TreatmentCharge,
            'RefiningCharge' => $this->RefiningCharge,
            'TobeHedged' => $this->TobeHedged,
            'TobePriced' => $this->TobePriced,
            'ContractualPayable' => $this->ContractualPayable,
            'PricedQuantity' => $this->PricedQuantity,
        ]);

        $query->andFilterWhere(['like', 'Measurement', $this->Measurement])
            ->andFilterWhere(['like', 'Unit', $this->Unit]);

        return $dataProvider;
    }
}
