<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_customers".
 *
 * @property integer $ID
 * @property string $CustomerName
 * @property string $SupplierConsumer
 *
 * @property MainContract[] $mainContracts
 */
class LookupCustomers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_customers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CustomerName', 'SupplierConsumer'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CustomerName' => 'Customer Name',
            'SupplierConsumer' => 'Supplier Consumer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainContracts()
    {
        return $this->hasMany(MainContract::className(), ['Counterparty' => 'ID']);
    }
}
