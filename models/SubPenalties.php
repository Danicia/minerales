<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_penalties".
 *
 * @property integer $ID
 * @property integer $Contract-ID
 * @property double $Amount
 * @property double $ForEach
 * @property double $Above
 * @property integer $Element
 * @property double $Penalty
 * @property double $Amount2
 * @property double $Above2
 * @property double $Amount3
 * @property double $Above3
 *
 * @property MainContract $contract-
 */
class SubPenalties extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_penalties';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'Contract-ID', 'Element'], 'integer'],
            [['Amount', 'ForEach', 'Above', 'Penalty', 'Amount2', 'Above2', 'Amount3', 'Above3'], 'number'],
            [['Contract-ID'], 'exist', 'skipOnError' => true, 'targetClass' => MainContract::className(), 'targetAttribute' => ['Contract-ID' => 'Contract-ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Contract-ID' => 'Contract  ID',
            'Amount' => 'Amount',
            'ForEach' => 'For Each',
            'Above' => 'Above',
            'Element' => 'Element',
            'Penalty' => 'Penalty',
            'Amount2' => 'Amount2',
            'Above2' => 'Above2',
            'Amount3' => 'Amount3',
            'Above3' => 'Above3',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(MainContract::className(), ['Contract-ID' => 'Contract-ID']);
    }
}
