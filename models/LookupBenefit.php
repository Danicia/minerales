<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_benefit".
 *
 * @property integer $ID
 * @property string $Benefit
 * @property string $Description
 *
 * @property SubBenefitcost[] $subBenefitcosts
 */
class LookupBenefit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_benefit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Description'], 'string'],
            [['Benefit'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Benefit' => 'Benefit',
            'Description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubBenefitcosts()
    {
        return $this->hasMany(SubBenefitcost::className(), ['BenefitCost' => 'ID']);
    }
}
