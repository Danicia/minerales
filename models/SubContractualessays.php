<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_contractualessays".
 *
 * @property integer $ID
 * @property integer $ContractID
 * @property integer $Element
 * @property string $Measurement
 * @property double $Minimum
 * @property double $Maximum
 * @property double $ApproxValue
 *
 * @property LookupElements $element
 * @property MainContract $contract
 */
class SubContractualessays extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_contractualessays';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ContractID', 'Element'], 'integer'],
            [['Minimum', 'Maximum', 'ApproxValue'], 'number'],
            [['Measurement'], 'string', 'max' => 255],
            [['Element'], 'exist', 'skipOnError' => true, 'targetClass' => LookupElements::className(), 'targetAttribute' => ['Element' => 'ID']],
            [['ContractID'], 'exist', 'skipOnError' => true, 'targetClass' => MainContract::className(), 'targetAttribute' => ['ContractID' => 'Contract-ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ContractID' => 'Contract ID',
            'Element' => 'Element',
            'Measurement' => 'Measurement',
            'Minimum' => 'Minimum',
            'Maximum' => 'Maximum',
            'ApproxValue' => 'Approx Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElement()
    {
        return $this->hasOne(LookupElements::className(), ['ID' => 'Element']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(MainContract::className(), ['Contract-ID' => 'ContractID']);
    }
}
