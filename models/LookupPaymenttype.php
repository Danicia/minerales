<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_paymenttype".
 *
 * @property integer $ID
 * @property string $Type
 *
 * @property SubPayments[] $subPayments
 */
class LookupPaymenttype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_paymenttype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubPayments()
    {
        return $this->hasMany(SubPayments::className(), ['Type' => 'ID']);
    }
}
