<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_loan".
 *
 * @property integer $ID
 * @property integer $ContractID
 * @property string $RepaymentDate
 * @property double $Principal
 * @property double $Interest
 * @property string $Value Date
 * @property double $AmountPaid
 * @property string $InvoiceNo
 * @property string $Comment
 * @property string $PaymentType
 * @property string $Type
 *
 * @property MainContract $contract
 */
class SubLoan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_loan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ContractID'], 'integer'],
            [['RepaymentDate', 'Value Date'], 'safe'],
            [['Principal', 'Interest', 'AmountPaid'], 'number'],
            [['InvoiceNo', 'Comment', 'PaymentType', 'Type'], 'string', 'max' => 255],
            [['ContractID'], 'exist', 'skipOnError' => true, 'targetClass' => MainContract::className(), 'targetAttribute' => ['ContractID' => 'Contract-ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ContractID' => 'Contract ID',
            'RepaymentDate' => 'Repayment Date',
            'Principal' => 'Principal',
            'Interest' => 'Interest',
            'Value Date' => 'Value  Date',
            'AmountPaid' => 'Amount Paid',
            'InvoiceNo' => 'Invoice No',
            'Comment' => 'Comment',
            'PaymentType' => 'Payment Type',
            'Type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(MainContract::className(), ['Contract-ID' => 'ContractID']);
    }
}
