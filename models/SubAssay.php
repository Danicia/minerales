<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_assay".
 *
 * @property integer $ID
 * @property integer $Quota-ID
 * @property integer $Element
 * @property double $ProvisionalValue
 * @property double $FinalValue
 * @property double $Umpire
 * @property integer $Calculation
 * @property double $Payable
 * @property double $Escalator
 * @property double $AveragePrice
 * @property double $TotalPriced
 * @property double $MaterialPrice
 * @property double $TotalUnpriced
 * @property double $Penalty
 * @property double $TotalHedged
 * @property double $AveragePriceHedged
 * @property double $TreatmentCharge
 * @property double $RefiningCharge
 * @property double $TobeHedged
 * @property string $Measurement
 * @property double $TobePriced
 * @property string $Unit
 * @property double $ContractualPayable
 * @property double $PricedQuantity
 *
 * @property LookupElements $element
 * @property MainQuota $quota-
 */
class SubAssay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_assay';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Quota-ID', 'Element', 'Calculation'], 'integer'],
            [['Element'], 'required'],
            [['ProvisionalValue', 'FinalValue', 'Umpire', 'Payable', 'Escalator', 'AveragePrice', 'TotalPriced', 'MaterialPrice', 'TotalUnpriced', 'Penalty', 'TotalHedged', 'AveragePriceHedged', 'TreatmentCharge', 'RefiningCharge', 'TobeHedged', 'TobePriced', 'ContractualPayable', 'PricedQuantity'], 'number'],
            [['Measurement', 'Unit'], 'string', 'max' => 255],
            [['Element'], 'exist', 'skipOnError' => true, 'targetClass' => LookupElements::className(), 'targetAttribute' => ['Element' => 'ID']],
            [['Quota-ID'], 'exist', 'skipOnError' => true, 'targetClass' => MainQuota::className(), 'targetAttribute' => ['Quota-ID' => 'Quota-ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Quota-ID' => 'Quota  ID',
            'Element' => 'Element',
            'ProvisionalValue' => 'Provisional Value',
            'FinalValue' => 'Final Value',
            'Umpire' => 'Umpire',
            'Calculation' => 'Calculation',
            'Payable' => 'Payable',
            'Escalator' => 'Escalator',
            'AveragePrice' => 'Average Price',
            'TotalPriced' => 'Total Priced',
            'MaterialPrice' => 'Material Price',
            'TotalUnpriced' => 'Total Unpriced',
            'Penalty' => 'Penalty',
            'TotalHedged' => 'Total Hedged',
            'AveragePriceHedged' => 'Average Price Hedged',
            'TreatmentCharge' => 'Treatment Charge',
            'RefiningCharge' => 'Refining Charge',
            'TobeHedged' => 'Tobe Hedged',
            'Measurement' => 'Measurement',
            'TobePriced' => 'Tobe Priced',
            'Unit' => 'Unit',
            'ContractualPayable' => 'Contractual Payable',
            'PricedQuantity' => 'Priced Quantity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElement()
    {
        return $this->hasOne(LookupElements::className(), ['ID' => 'Element']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuota()
    {
        return $this->hasOne(MainQuota::className(), ['Quota-ID' => 'Quota-ID']);
    }
}
