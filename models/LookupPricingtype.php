<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_pricingtype".
 *
 * @property integer $ID
 * @property string $Type
 *
 * @property SubPricing[] $subPricings
 */
class LookupPricingtype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_pricingtype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubPricings()
    {
        return $this->hasMany(SubPricing::className(), ['Type' => 'ID']);
    }
}
