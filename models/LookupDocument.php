<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_document".
 *
 * @property integer $ID
 * @property string $Document
 * @property string $Description
 *
 * @property SubDispatch[] $subDispatches
 */
class LookupDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Document'], 'string', 'max' => 25],
            [['Description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Document' => 'Document',
            'Description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubDispatches()
    {
        return $this->hasMany(SubDispatch::className(), ['Document' => 'ID']);
    }
}
