<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_tolerance".
 *
 * @property integer $ID
 * @property double $Tolerance
 */
class LookupTolerance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_tolerance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Tolerance'], 'required'],
            [['Tolerance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Tolerance' => 'Tolerance',
        ];
    }
}
