<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MainContract;

/**
 * MainContractSearch represents the model behind the search form about `app\models\MainContract`.
 */
class MainContractSearch extends MainContract
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Contract-ID', 'Counterparty', 'Material', 'Tolerance', 'Delivery', 'NumberOfQuotas', 'Finalized', 'Spezification', 'ContractNumberFix'], 'integer'],
            [['PS', 'ContractualAssays', 'ContractDate', 'ContractNumber', 'Comment', 'Payment', 'CustomerRef'], 'safe'],
            [['TotalContractualBaseMt', 'Franchise', 'ProvisionalPayment', 'FinalPayment', 'ContractualMoisture', 'Loan'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MainContract::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Contract-ID' => $this->Contract-ID,
            'Counterparty' => $this->Counterparty,
            'Material' => $this->Material,
            'Tolerance' => $this->Tolerance,
            'ContractDate' => $this->ContractDate,
            'TotalContractualBaseMt' => $this->TotalContractualBaseMt,
            'Delivery' => $this->Delivery,
            'NumberOfQuotas' => $this->NumberOfQuotas,
            'Franchise' => $this->Franchise,
            'ProvisionalPayment' => $this->ProvisionalPayment,
            'FinalPayment' => $this->FinalPayment,
            'Finalized' => $this->Finalized,
            'Spezification' => $this->Spezification,
            'ContractualMoisture' => $this->ContractualMoisture,
            'Loan' => $this->Loan,
            'ContractNumberFix' => $this->ContractNumberFix,
        ]);

        $query->andFilterWhere(['like', 'PS', $this->PS])
            ->andFilterWhere(['like', 'ContractualAssays', $this->ContractualAssays])
            ->andFilterWhere(['like', 'ContractNumber', $this->ContractNumber])
            ->andFilterWhere(['like', 'Comment', $this->Comment])
            ->andFilterWhere(['like', 'Payment', $this->Payment])
            ->andFilterWhere(['like', 'CustomerRef', $this->CustomerRef]);

        return $dataProvider;
    }
}
