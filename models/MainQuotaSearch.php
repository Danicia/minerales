<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MainQuota;

/**
 * MainQuotaSearch represents the model behind the search form about `app\models\MainQuota`.
 */
class MainQuotaSearch extends MainQuota
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Quota-ID', 'Contract-ID', 'FinalWeight', 'ExecutionNumberFixed', 'FinancingBank', 'DeliveryLocation', 'Quality'], 'integer'],
            [['QuotaMonth', 'LotNumber', 'CustomerLetRef', 'ExecutionNumber'], 'safe'],
            [['WMT', 'Moisture', 'DMT', 'PayableDMT', 'Franchise'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MainQuota::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Quota-ID' => $this->Quota-ID,
            'Contract-ID' => $this->Contract-ID,
            'QuotaMonth' => $this->QuotaMonth,
            'WMT' => $this->WMT,
            'Moisture' => $this->Moisture,
            'DMT' => $this->DMT,
            'FinalWeight' => $this->FinalWeight,
            'PayableDMT' => $this->PayableDMT,
            'Franchise' => $this->Franchise,
            'ExecutionNumberFixed' => $this->ExecutionNumberFixed,
            'FinancingBank' => $this->FinancingBank,
            'DeliveryLocation' => $this->DeliveryLocation,
            'Quality' => $this->Quality,
        ]);

        $query->andFilterWhere(['like', 'LotNumber', $this->LotNumber])
            ->andFilterWhere(['like', 'CustomerLetRef', $this->CustomerLetRef])
            ->andFilterWhere(['like', 'ExecutionNumber', $this->ExecutionNumber]);

        return $dataProvider;
    }
}
