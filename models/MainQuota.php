<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "main_quota".
 *
 * @property integer $Quota-ID
 * @property integer $Contract-ID
 * @property string $QuotaMonth
 * @property string $LotNumber
 * @property double $WMT
 * @property double $Moisture
 * @property double $DMT
 * @property integer $FinalWeight
 * @property double $PayableDMT
 * @property double $Franchise
 * @property string $CustomerLetRef
 * @property string $ExecutionNumber
 * @property integer $ExecutionNumberFixed
 * @property integer $FinancingBank
 * @property integer $DeliveryLocation
 * @property integer $Quality
 *
 * @property LookupBanks $financingBank
 * @property LookupDeliveries $deliveryLocation
 * @property MainContract $contract-
 * @property SubAllocation[] $subAllocations
 * @property SubAssay[] $subAssays
 * @property SubBenefitcost[] $subBenefitcosts
 * @property SubDispatch[] $subDispatches
 * @property SubPayments[] $subPayments
 * @property SubPaymentsopex[] $subPaymentsopexes
 * @property SubPricing[] $subPricings
 */
class MainQuota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_quota';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Contract-ID', 'QuotaMonth', 'Moisture', 'FinalWeight', 'ExecutionNumberFixed'], 'required'],
            [['Contract-ID', 'FinalWeight', 'ExecutionNumberFixed', 'FinancingBank', 'DeliveryLocation', 'Quality'], 'integer'],
            [['QuotaMonth'], 'safe'],
            [['WMT', 'Moisture', 'DMT', 'PayableDMT', 'Franchise'], 'number'],
            [['LotNumber', 'CustomerLetRef', 'ExecutionNumber'], 'string', 'max' => 255],
            [['FinancingBank'], 'exist', 'skipOnError' => true, 'targetClass' => LookupBanks::className(), 'targetAttribute' => ['FinancingBank' => 'ID']],
            [['DeliveryLocation'], 'exist', 'skipOnError' => true, 'targetClass' => LookupDeliveries::className(), 'targetAttribute' => ['DeliveryLocation' => 'ID']],
            [['Contract-ID'], 'exist', 'skipOnError' => true, 'targetClass' => MainContract::className(), 'targetAttribute' => ['Contract-ID' => 'Contract-ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Quota-ID' => 'Quota  ID',
            'Contract-ID' => 'Contract  ID',
            'QuotaMonth' => 'Quota Month',
            'LotNumber' => 'Lot Number',
            'WMT' => 'Wmt',
            'Moisture' => 'Moisture',
            'DMT' => 'Dmt',
            'FinalWeight' => 'Final Weight',
            'PayableDMT' => 'Payable Dmt',
            'Franchise' => 'Franchise',
            'CustomerLetRef' => 'Customer Let Ref',
            'ExecutionNumber' => 'Execution Number',
            'ExecutionNumberFixed' => 'Execution Number Fixed',
            'FinancingBank' => 'Financing Bank',
            'DeliveryLocation' => 'Delivery Location',
            'Quality' => 'Quality',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancingBank()
    {
        return $this->hasOne(LookupBanks::className(), ['ID' => 'FinancingBank']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryLocation()
    {
        return $this->hasOne(LookupDeliveries::className(), ['ID' => 'DeliveryLocation']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(MainContract::className(), ['Contract-ID' => 'Contract-ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubAllocations()
    {
        return $this->hasMany(SubAllocation::className(), ['Quota-ID' => 'Quota-ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubAssays()
    {
        return $this->hasMany(SubAssay::className(), ['Quota-ID' => 'Quota-ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubBenefitcosts()
    {
        return $this->hasMany(SubBenefitcost::className(), ['Quota-ID' => 'Quota-ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubDispatches()
    {
        return $this->hasMany(SubDispatch::className(), ['Quota-ID' => 'Quota-ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubPayments()
    {
        return $this->hasMany(SubPayments::className(), ['Quota-ID' => 'Quota-ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubPaymentsopexes()
    {
        return $this->hasMany(SubPaymentsopex::className(), ['Quota-ID' => 'Quota-ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubPricings()
    {
        return $this->hasMany(SubPricing::className(), ['Quota-ID' => 'Quota-ID']);
    }
}
