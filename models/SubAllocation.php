<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_allocation".
 *
 * @property integer $Quota-ID
 * @property integer $Quota-IDPurchase
 * @property string $LotNumber
 * @property string $Contract
 * @property string $Supplier
 * @property string $DeliveryDate
 * @property double $WMT
 * @property double $DMT
 * @property double $DMTPayable
 * @property integer $Delivered
 * @property double $MaterialValue
 * @property string $Comment
 * @property string $Pricing-ID
 * @property string $LatestPricingDate
 * @property integer $Priced
 * @property integer $Paid
 * @property integer $DispatchIDPurchase
 * @property integer $ID
 *
 * @property MainQuota $quota-
 */
class SubAllocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_allocation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Quota-ID', 'Delivered', 'Priced', 'Paid'], 'required'],
            [['Quota-ID', 'Quota-IDPurchase', 'Delivered', 'Priced', 'Paid', 'DispatchIDPurchase'], 'integer'],
            [['DeliveryDate', 'LatestPricingDate'], 'safe'],
            [['WMT', 'DMT', 'DMTPayable', 'MaterialValue'], 'number'],
            [['Comment', 'Pricing-ID'], 'string'],
            [['LotNumber', 'Contract', 'Supplier'], 'string', 'max' => 255],
            [['Quota-ID'], 'exist', 'skipOnError' => true, 'targetClass' => MainQuota::className(), 'targetAttribute' => ['Quota-ID' => 'Quota-ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Quota-ID' => 'Quota  ID',
            'Quota-IDPurchase' => 'Quota  Idpurchase',
            'LotNumber' => 'Lot Number',
            'Contract' => 'Contract',
            'Supplier' => 'Supplier',
            'DeliveryDate' => 'Delivery Date',
            'WMT' => 'Wmt',
            'DMT' => 'Dmt',
            'DMTPayable' => 'Dmtpayable',
            'Delivered' => 'Delivered',
            'MaterialValue' => 'Material Value',
            'Comment' => 'Comment',
            'Pricing-ID' => 'Pricing  ID',
            'LatestPricingDate' => 'Latest Pricing Date',
            'Priced' => 'Priced',
            'Paid' => 'Paid',
            'DispatchIDPurchase' => 'Dispatch Idpurchase',
            'ID' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuota()
    {
        return $this->hasOne(MainQuota::className(), ['Quota-ID' => 'Quota-ID']);
    }
}
