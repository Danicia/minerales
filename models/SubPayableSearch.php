<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubPayable;

/**
 * SubPayableSearch represents the model behind the search form about `app\models\SubPayable`.
 */
class SubPayableSearch extends SubPayable
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Contract-ID', 'Commodity', 'PayableUnits', 'BasePrice', 'Escalator', 'CalculationType'], 'integer'],
            [['DeductionPriorPayable', 'Payable', 'MinDeduction', 'TreatmentCharge', 'RefiningCharge', 'UpEscalator', 'DownEscalator'], 'number'],
            [['Measurement', 'RCBasis'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubPayable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Contract-ID' => $this->Contract-ID,
            'Commodity' => $this->Commodity,
            'DeductionPriorPayable' => $this->DeductionPriorPayable,
            'Payable' => $this->Payable,
            'MinDeduction' => $this->MinDeduction,
            'PayableUnits' => $this->PayableUnits,
            'TreatmentCharge' => $this->TreatmentCharge,
            'RefiningCharge' => $this->RefiningCharge,
            'BasePrice' => $this->BasePrice,
            'UpEscalator' => $this->UpEscalator,
            'DownEscalator' => $this->DownEscalator,
            'Escalator' => $this->Escalator,
            'CalculationType' => $this->CalculationType,
        ]);

        $query->andFilterWhere(['like', 'Measurement', $this->Measurement])
            ->andFilterWhere(['like', 'RCBasis', $this->RCBasis]);

        return $dataProvider;
    }
}
