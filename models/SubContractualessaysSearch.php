<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubContractualessays;

/**
 * SubContractualessaysSearch represents the model behind the search form about `app\models\SubContractualessays`.
 */
class SubContractualessaysSearch extends SubContractualessays
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ContractID', 'Element'], 'integer'],
            [['Measurement'], 'safe'],
            [['Minimum', 'Maximum', 'ApproxValue'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubContractualessays::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'ContractID' => $this->ContractID,
            'Element' => $this->Element,
            'Minimum' => $this->Minimum,
            'Maximum' => $this->Maximum,
            'ApproxValue' => $this->ApproxValue,
        ]);

        $query->andFilterWhere(['like', 'Measurement', $this->Measurement]);

        return $dataProvider;
    }
}
