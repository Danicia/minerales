<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubAllocation;

/**
 * SubAllocationSearch represents the model behind the search form about `app\models\SubAllocation`.
 */
class SubAllocationSearch extends SubAllocation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Quota-ID', 'Quota-IDPurchase', 'Delivered', 'Priced', 'Paid', 'DispatchIDPurchase', 'ID'], 'integer'],
            [['LotNumber', 'Contract', 'Supplier', 'DeliveryDate', 'Comment', 'Pricing-ID', 'LatestPricingDate'], 'safe'],
            [['WMT', 'DMT', 'DMTPayable', 'MaterialValue'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubAllocation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Quota-ID' => $this->Quota-ID,
            'Quota-IDPurchase' => $this->Quota-IDPurchase,
            'DeliveryDate' => $this->DeliveryDate,
            'WMT' => $this->WMT,
            'DMT' => $this->DMT,
            'DMTPayable' => $this->DMTPayable,
            'Delivered' => $this->Delivered,
            'MaterialValue' => $this->MaterialValue,
            'LatestPricingDate' => $this->LatestPricingDate,
            'Priced' => $this->Priced,
            'Paid' => $this->Paid,
            'DispatchIDPurchase' => $this->DispatchIDPurchase,
            'ID' => $this->ID,
        ]);

        $query->andFilterWhere(['like', 'LotNumber', $this->LotNumber])
            ->andFilterWhere(['like', 'Contract', $this->Contract])
            ->andFilterWhere(['like', 'Supplier', $this->Supplier])
            ->andFilterWhere(['like', 'Comment', $this->Comment])
            ->andFilterWhere(['like', 'Pricing-ID', $this->Pricing-ID]);

        return $dataProvider;
    }
}
