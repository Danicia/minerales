<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_elements".
 *
 * @property integer $ID
 * @property string $Elements
 * @property string $Description
 * @property double $PayableBorder
 * @property string $PayableCalcVersion
 * @property double $MarketPrice
 * @property string $Measurement
 * @property string $Unit
 *
 * @property SubAssay[] $subAssays
 * @property SubContractualessays[] $subContractualessays
 * @property SubPricing[] $subPricings
 */
class LookupElements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_elements';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PayableBorder', 'MarketPrice'], 'number'],
            [['Elements'], 'string', 'max' => 10],
            [['Description'], 'string', 'max' => 25],
            [['PayableCalcVersion', 'Measurement', 'Unit'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Elements' => 'Elements',
            'Description' => 'Description',
            'PayableBorder' => 'Payable Border',
            'PayableCalcVersion' => 'Payable Calc Version',
            'MarketPrice' => 'Market Price',
            'Measurement' => 'Measurement',
            'Unit' => 'Unit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubAssays()
    {
        return $this->hasMany(SubAssay::className(), ['Element' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubContractualessays()
    {
        return $this->hasMany(SubContractualessays::className(), ['Element' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubPricings()
    {
        return $this->hasMany(SubPricing::className(), ['Element' => 'ID']);
    }
}
