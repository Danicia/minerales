<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_calculationtype".
 *
 * @property integer $ID
 * @property string $CalculationType
 *
 * @property SubPayable[] $subPayables
 */
class LookupCalculationtype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_calculationtype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID'], 'integer'],
            [['CalculationType'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CalculationType' => 'Calculation Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubPayables()
    {
        return $this->hasMany(SubPayable::className(), ['CalculationType' => 'ID']);
    }
}
