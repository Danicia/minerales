<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_hedging".
 *
 * @property string $TradeDate
 * @property string $PromptDate
 * @property integer $Element
 * @property double $Quantity
 * @property double $Price
 * @property string $Unit
 * @property integer $Quota-ID
 * @property string $BrokerRef
 * @property integer $ID
 */
class SubHedging extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_hedging';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TradeDate', 'PromptDate'], 'safe'],
            [['Element'], 'required'],
            [['Element', 'Quota-ID'], 'integer'],
            [['Quantity', 'Price'], 'number'],
            [['Unit', 'BrokerRef'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'TradeDate' => 'Trade Date',
            'PromptDate' => 'Prompt Date',
            'Element' => 'Element',
            'Quantity' => 'Quantity',
            'Price' => 'Price',
            'Unit' => 'Unit',
            'Quota-ID' => 'Quota  ID',
            'BrokerRef' => 'Broker Ref',
            'ID' => 'ID',
        ];
    }
}
