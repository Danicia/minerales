<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_material".
 *
 * @property integer $ID
 * @property string $Material
 * @property string $Abb
 *
 * @property MainContract[] $mainContracts
 */
class LookupMaterial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_material';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Material', 'Abb'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Material' => 'Material',
            'Abb' => 'Abb',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainContracts()
    {
        return $this->hasMany(MainContract::className(), ['Material' => 'ID']);
    }
}
