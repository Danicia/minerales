<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_deliveries".
 *
 * @property integer $ID
 * @property string $Delivery
 *
 * @property MainContract[] $mainContracts
 * @property MainQuota[] $mainQuotas
 */
class LookupDeliveries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_deliveries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Delivery'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Delivery' => 'Delivery',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainContracts()
    {
        return $this->hasMany(MainContract::className(), ['Delivery' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainQuotas()
    {
        return $this->hasMany(MainQuota::className(), ['DeliveryLocation' => 'ID']);
    }
}
