<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_payable".
 *
 * @property integer $ID
 * @property integer $Contract-ID
 * @property integer $Commodity
 * @property double $DeductionPriorPayable
 * @property double $Payable
 * @property double $MinDeduction
 * @property integer $PayableUnits
 * @property double $TreatmentCharge
 * @property double $RefiningCharge
 * @property integer $BasePrice
 * @property double $UpEscalator
 * @property double $DownEscalator
 * @property integer $Escalator
 * @property string $Measurement
 * @property string $RCBasis
 * @property integer $CalculationType
 *
 * @property LookupCalculationtype $calculationType
 */
class SubPayable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_payable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Contract-ID', 'Commodity', 'PayableUnits', 'BasePrice', 'Escalator', 'CalculationType'], 'integer'],
            [['DeductionPriorPayable', 'Payable', 'MinDeduction', 'TreatmentCharge', 'RefiningCharge', 'UpEscalator', 'DownEscalator'], 'number'],
            [['Escalator'], 'required'],
            [['Measurement', 'RCBasis'], 'string', 'max' => 255],
            [['CalculationType'], 'exist', 'skipOnError' => true, 'targetClass' => LookupCalculationtype::className(), 'targetAttribute' => ['CalculationType' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Contract-ID' => 'Contract  ID',
            'Commodity' => 'Commodity',
            'DeductionPriorPayable' => 'Deduction Prior Payable',
            'Payable' => 'Payable',
            'MinDeduction' => 'Min Deduction',
            'PayableUnits' => 'Payable Units',
            'TreatmentCharge' => 'Treatment Charge',
            'RefiningCharge' => 'Refining Charge',
            'BasePrice' => 'Base Price',
            'UpEscalator' => 'Up Escalator',
            'DownEscalator' => 'Down Escalator',
            'Escalator' => 'Escalator',
            'Measurement' => 'Measurement',
            'RCBasis' => 'Rcbasis',
            'CalculationType' => 'Calculation Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalculationType()
    {
        return $this->hasOne(LookupCalculationtype::className(), ['ID' => 'CalculationType']);
    }
}
