<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_tolerance".
 *
 * @property integer $ID
 * @property integer $ContractID
 * @property double $Quantity
 * @property string $Sign
 * @property string $Unit
 * @property double $ToleranceValue
 *
 * @property MainContract $contract
 */
class SubTolerance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_tolerance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ContractID'], 'integer'],
            [['Quantity', 'ToleranceValue'], 'number'],
            [['Sign', 'Unit'], 'string', 'max' => 255],
            [['ContractID'], 'exist', 'skipOnError' => true, 'targetClass' => MainContract::className(), 'targetAttribute' => ['ContractID' => 'Contract-ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ContractID' => 'Contract ID',
            'Quantity' => 'Quantity',
            'Sign' => 'Sign',
            'Unit' => 'Unit',
            'ToleranceValue' => 'Tolerance Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(MainContract::className(), ['Contract-ID' => 'ContractID']);
    }
}
