<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup_paymenttypeopex".
 *
 * @property integer $ID
 * @property string $Type
 *
 * @property SubPaymentsopex[] $subPaymentsopexes
 */
class LookupPaymenttypeopex extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_paymenttypeopex';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubPaymentsopexes()
    {
        return $this->hasMany(SubPaymentsopex::className(), ['Type' => 'ID']);
    }
}
