<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_benefitcost".
 *
 * @property integer $Quota-ID
 * @property integer $BenefitCost
 * @property double $AmountWMT
 * @property double $AmountDMT
 * @property integer $CalculateDMT
 * @property integer $GetFromContract
 * @property integer $NotContractLinkedCosts
 * @property integer $ID
 *
 * @property LookupBenefit $benefitCost
 * @property MainQuota $quota-
 */
class SubBenefitcost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_benefitcost';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Quota-ID', 'BenefitCost', 'CalculateDMT', 'GetFromContract', 'NotContractLinkedCosts'], 'integer'],
            [['AmountWMT', 'AmountDMT'], 'number'],
            [['CalculateDMT', 'GetFromContract', 'NotContractLinkedCosts'], 'required'],
            [['BenefitCost'], 'exist', 'skipOnError' => true, 'targetClass' => LookupBenefit::className(), 'targetAttribute' => ['BenefitCost' => 'ID']],
            [['Quota-ID'], 'exist', 'skipOnError' => true, 'targetClass' => MainQuota::className(), 'targetAttribute' => ['Quota-ID' => 'Quota-ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Quota-ID' => 'Quota  ID',
            'BenefitCost' => 'Benefit Cost',
            'AmountWMT' => 'Amount Wmt',
            'AmountDMT' => 'Amount Dmt',
            'CalculateDMT' => 'Calculate Dmt',
            'GetFromContract' => 'Get From Contract',
            'NotContractLinkedCosts' => 'Not Contract Linked Costs',
            'ID' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBenefitCost()
    {
        return $this->hasOne(LookupBenefit::className(), ['ID' => 'BenefitCost']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuota()
    {
        return $this->hasOne(MainQuota::className(), ['Quota-ID' => 'Quota-ID']);
    }
}
