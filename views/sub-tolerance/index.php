<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Tolerances';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-tolerance-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Sub Tolerance', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'ContractID',
            'Quantity',
            'Sign',
            'Unit',
            // 'ToleranceValue',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
