<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubTolerance */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-tolerance-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ContractID')->textInput() ?>

    <?= $form->field($model, 'Quantity')->textInput() ?>

    <?= $form->field($model, 'Sign')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ToleranceValue')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
