<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubTolerance */

$this->title = 'Update Sub Tolerance: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Sub Tolerances', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sub-tolerance-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
