<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubTolerance */

$this->title = 'Create Sub Tolerance';
$this->params['breadcrumbs'][] = ['label' => 'Sub Tolerances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-tolerance-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
