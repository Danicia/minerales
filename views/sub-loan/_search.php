<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubLoanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-loan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'ContractID') ?>

    <?= $form->field($model, 'RepaymentDate') ?>

    <?= $form->field($model, 'Principal') ?>

    <?= $form->field($model, 'Interest') ?>

    <?php // echo $form->field($model, 'Value Date') ?>

    <?php // echo $form->field($model, 'AmountPaid') ?>

    <?php // echo $form->field($model, 'InvoiceNo') ?>

    <?php // echo $form->field($model, 'Comment') ?>

    <?php // echo $form->field($model, 'PaymentType') ?>

    <?php // echo $form->field($model, 'Type') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
