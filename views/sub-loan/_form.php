<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubLoan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-loan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ContractID')->textInput() ?>

    <?= $form->field($model, 'RepaymentDate')->textInput() ?>

    <?= $form->field($model, 'Principal')->textInput() ?>

    <?= $form->field($model, 'Interest')->textInput() ?>

    <?= $form->field($model, 'Value Date')->textInput() ?>

    <?= $form->field($model, 'AmountPaid')->textInput() ?>

    <?= $form->field($model, 'InvoiceNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PaymentType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Type')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
