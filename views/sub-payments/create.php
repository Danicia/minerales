<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubPayments */

$this->title = 'Create Sub Payments';
$this->params['breadcrumbs'][] = ['label' => 'Sub Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-payments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
