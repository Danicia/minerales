<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubPayments */

$this->title = 'Update Sub Payments: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Sub Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sub-payments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
