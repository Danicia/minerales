<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LookupDeliveriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lookup Deliveries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-deliveries-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Lookup Deliveries', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'Delivery',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
