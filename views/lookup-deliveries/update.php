<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LookupDeliveries */

$this->title = 'Update Lookup Deliveries: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Lookup Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lookup-deliveries-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
