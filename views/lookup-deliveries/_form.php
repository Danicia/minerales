<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LookupDeliveries */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lookup-deliveries-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Delivery')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
