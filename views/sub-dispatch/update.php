<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubDispatch */

$this->title = 'Update Sub Dispatch: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Sub Dispatches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sub-dispatch-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
