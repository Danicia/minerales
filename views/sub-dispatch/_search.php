<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubDispatchSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-dispatch-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Quota-ID') ?>

    <?= $form->field($model, 'TruckPlate') ?>

    <?= $form->field($model, 'Departure') ?>

    <?= $form->field($model, 'DepartureETA') ?>

    <?php // echo $form->field($model, 'DepartureLocation') ?>

    <?php // echo $form->field($model, 'ArrivalOruro') ?>

    <?php // echo $form->field($model, 'ArrivalOruroETA') ?>

    <?php // echo $form->field($model, 'Arrival') ?>

    <?php // echo $form->field($model, 'ArrivalETA') ?>

    <?php // echo $form->field($model, 'ArrivalLocation') ?>

    <?php // echo $form->field($model, 'WMT') ?>

    <?php // echo $form->field($model, 'DMT') ?>

    <?php // echo $form->field($model, 'DMTPayable') ?>

    <?php // echo $form->field($model, 'Document') ?>

    <?php // echo $form->field($model, 'MaterialValue') ?>

    <?php // echo $form->field($model, 'Control') ?>

    <?php // echo $form->field($model, 'Comment') ?>

    <?php // echo $form->field($model, 'Pricing-ID') ?>

    <?php // echo $form->field($model, 'LatestPricingDate') ?>

    <?php // echo $form->field($model, 'Priced') ?>

    <?php // echo $form->field($model, 'WarehouseType') ?>

    <?php // echo $form->field($model, 'Delivered') ?>

    <?php // echo $form->field($model, 'Transport') ?>

    <?php // echo $form->field($model, 'NoOfTrucks') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
