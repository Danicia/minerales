<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubDispatchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Dispatches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-dispatch-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sub Dispatch', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'Quota-ID',
            'TruckPlate',
            'Departure',
            'DepartureETA',
            // 'DepartureLocation',
            // 'ArrivalOruro',
            // 'ArrivalOruroETA',
            // 'Arrival',
            // 'ArrivalETA',
            // 'ArrivalLocation',
            // 'WMT',
            // 'DMT',
            // 'DMTPayable',
            // 'Document',
            // 'MaterialValue',
            // 'Control',
            // 'Comment:ntext',
            // 'Pricing-ID:ntext',
            // 'LatestPricingDate',
            // 'Priced',
            // 'WarehouseType',
            // 'Delivered',
            // 'Transport',
            // 'NoOfTrucks',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
