<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubDispatch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-dispatch-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Quota-ID')->textInput() ?>

    <?= $form->field($model, 'TruckPlate')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Departure')->textInput() ?>

    <?= $form->field($model, 'DepartureETA')->textInput() ?>

    <?= $form->field($model, 'DepartureLocation')->textInput() ?>

    <?= $form->field($model, 'ArrivalOruro')->textInput() ?>

    <?= $form->field($model, 'ArrivalOruroETA')->textInput() ?>

    <?= $form->field($model, 'Arrival')->textInput() ?>

    <?= $form->field($model, 'ArrivalETA')->textInput() ?>

    <?= $form->field($model, 'ArrivalLocation')->textInput() ?>

    <?= $form->field($model, 'WMT')->textInput() ?>

    <?= $form->field($model, 'DMT')->textInput() ?>

    <?= $form->field($model, 'DMTPayable')->textInput() ?>

    <?= $form->field($model, 'Document')->textInput() ?>

    <?= $form->field($model, 'MaterialValue')->textInput() ?>

    <?= $form->field($model, 'Control')->textInput() ?>

    <?= $form->field($model, 'Comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Pricing-ID')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'LatestPricingDate')->textInput() ?>

    <?= $form->field($model, 'Priced')->textInput() ?>

    <?= $form->field($model, 'WarehouseType')->textInput() ?>

    <?= $form->field($model, 'Delivered')->textInput() ?>

    <?= $form->field($model, 'Transport')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NoOfTrucks')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
