<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubDispatch */

$this->title = 'Create Sub Dispatch';
$this->params['breadcrumbs'][] = ['label' => 'Sub Dispatches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-dispatch-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
