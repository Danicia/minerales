<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubPricing */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-pricing-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Quota-ID')->textInput() ?>

    <?= $form->field($model, 'Element')->textInput() ?>

    <?= $form->field($model, 'PricingDate')->textInput() ?>

    <?= $form->field($model, 'Price')->textInput() ?>

    <?= $form->field($model, 'Quantity')->textInput() ?>

    <?= $form->field($model, 'Type')->textInput() ?>

    <?= $form->field($model, 'Measurement')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
