<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubPricing */

$this->title = 'Create Sub Pricing';
$this->params['breadcrumbs'][] = ['label' => 'Sub Pricings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-pricing-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
