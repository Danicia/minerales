<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubPaymentsopex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-paymentsopex-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Quota-ID')->textInput() ?>

    <?= $form->field($model, 'PaymentRef')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PaymentDate')->textInput() ?>

    <?= $form->field($model, 'Amount')->textInput() ?>

    <?= $form->field($model, 'Type')->textInput() ?>

    <?= $form->field($model, 'Comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PayementType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DueDate')->textInput() ?>

    <?= $form->field($model, 'InvoiceNumber')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
