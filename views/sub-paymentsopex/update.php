<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubPaymentsopex */

$this->title = 'Update Sub Paymentsopex: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Sub Paymentsopexes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sub-paymentsopex-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
