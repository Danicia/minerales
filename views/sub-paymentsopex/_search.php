<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubPaymentsopexSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-paymentsopex-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Quota-ID') ?>

    <?= $form->field($model, 'PaymentRef') ?>

    <?= $form->field($model, 'PaymentDate') ?>

    <?= $form->field($model, 'Amount') ?>

    <?php // echo $form->field($model, 'Type') ?>

    <?php // echo $form->field($model, 'Comment') ?>

    <?php // echo $form->field($model, 'PayementType') ?>

    <?php // echo $form->field($model, 'DueDate') ?>

    <?php // echo $form->field($model, 'InvoiceNumber') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
