<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubPenalties */

$this->title = 'Create Sub Penalties';
$this->params['breadcrumbs'][] = ['label' => 'Sub Penalties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-penalties-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
