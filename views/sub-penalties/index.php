<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Penalties';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-penalties-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Sub Penalties', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'Contract-ID',
            'Amount',
            'ForEach',
            'Above',
            // 'Element',
            // 'Penalty',
            // 'Amount2',
            // 'Above2',
            // 'Amount3',
            // 'Above3',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
