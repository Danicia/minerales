<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubPenalties */

$this->title = 'Update Sub Penalties: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Sub Penalties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sub-penalties-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
