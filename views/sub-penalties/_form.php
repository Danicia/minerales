<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubPenalties */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-penalties-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID')->textInput() ?>

    <?= $form->field($model, 'Contract-ID')->textInput() ?>

    <?= $form->field($model, 'Amount')->textInput() ?>

    <?= $form->field($model, 'ForEach')->textInput() ?>

    <?= $form->field($model, 'Above')->textInput() ?>

    <?= $form->field($model, 'Element')->textInput() ?>

    <?= $form->field($model, 'Penalty')->textInput() ?>

    <?= $form->field($model, 'Amount2')->textInput() ?>

    <?= $form->field($model, 'Above2')->textInput() ?>

    <?= $form->field($model, 'Amount3')->textInput() ?>

    <?= $form->field($model, 'Above3')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
