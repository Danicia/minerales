<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MainContractSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-contract-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Contract-ID') ?>

    <?= $form->field($model, 'PS') ?>

    <?= $form->field($model, 'Counterparty') ?>

    <?= $form->field($model, 'Material') ?>

    <?= $form->field($model, 'ContractualAssays') ?>

    <?php // echo $form->field($model, 'Tolerance') ?>

    <?php // echo $form->field($model, 'ContractDate') ?>

    <?php // echo $form->field($model, 'ContractNumber') ?>

    <?php // echo $form->field($model, 'TotalContractualBaseMt') ?>

    <?php // echo $form->field($model, 'Delivery') ?>

    <?php // echo $form->field($model, 'Comment') ?>

    <?php // echo $form->field($model, 'NumberOfQuotas') ?>

    <?php // echo $form->field($model, 'Franchise') ?>

    <?php // echo $form->field($model, 'Payment') ?>

    <?php // echo $form->field($model, 'ProvisionalPayment') ?>

    <?php // echo $form->field($model, 'FinalPayment') ?>

    <?php // echo $form->field($model, 'Finalized') ?>

    <?php // echo $form->field($model, 'Spezification') ?>

    <?php // echo $form->field($model, 'CustomerRef') ?>

    <?php // echo $form->field($model, 'ContractualMoisture') ?>

    <?php // echo $form->field($model, 'Loan') ?>

    <?php // echo $form->field($model, 'ContractNumberFix') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
