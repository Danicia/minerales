<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MainContract */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-contract-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'PS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Counterparty')->textInput() ?>

    <?= $form->field($model, 'Material')->textInput() ?>

    <?= $form->field($model, 'ContractualAssays')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Tolerance')->textInput() ?>

    <?= $form->field($model, 'ContractDate')->textInput() ?>

    <?= $form->field($model, 'ContractNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TotalContractualBaseMt')->textInput() ?>

    <?= $form->field($model, 'Delivery')->textInput() ?>

    <?= $form->field($model, 'Comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'NumberOfQuotas')->textInput() ?>

    <?= $form->field($model, 'Franchise')->textInput() ?>

    <?= $form->field($model, 'Payment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ProvisionalPayment')->textInput() ?>

    <?= $form->field($model, 'FinalPayment')->textInput() ?>

    <?= $form->field($model, 'Finalized')->textInput() ?>

    <?= $form->field($model, 'Spezification')->textInput() ?>

    <?= $form->field($model, 'CustomerRef')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ContractualMoisture')->textInput() ?>

    <?= $form->field($model, 'Loan')->textInput() ?>

    <?= $form->field($model, 'ContractNumberFix')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
