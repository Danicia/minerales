<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MainContractSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Main Contracts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-contract-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Main Contract', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Contract-ID',
            'PS',
            'Counterparty',
            'Material',
            'ContractualAssays:ntext',
            // 'Tolerance',
            // 'ContractDate',
            // 'ContractNumber',
            // 'TotalContractualBaseMt',
            // 'Delivery',
            // 'Comment:ntext',
            // 'NumberOfQuotas',
            // 'Franchise',
            // 'Payment',
            // 'ProvisionalPayment',
            // 'FinalPayment',
            // 'Finalized',
            // 'Spezification',
            // 'CustomerRef',
            // 'ContractualMoisture',
            // 'Loan',
            // 'ContractNumberFix',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
