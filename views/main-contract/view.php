<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MainContract */

$this->title = $model->Contract-ID;
$this->params['breadcrumbs'][] = ['label' => 'Main Contracts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-contract-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Contract-ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Contract-ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Contract-ID',
            'PS',
            'Counterparty',
            'Material',
            'ContractualAssays:ntext',
            'Tolerance',
            'ContractDate',
            'ContractNumber',
            'TotalContractualBaseMt',
            'Delivery',
            'Comment:ntext',
            'NumberOfQuotas',
            'Franchise',
            'Payment',
            'ProvisionalPayment',
            'FinalPayment',
            'Finalized',
            'Spezification',
            'CustomerRef',
            'ContractualMoisture',
            'Loan',
            'ContractNumberFix',
        ],
    ]) ?>

</div>
