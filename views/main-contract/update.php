<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MainContract */

$this->title = 'Update Main Contract: ' . $model->Contract-ID;
$this->params['breadcrumbs'][] = ['label' => 'Main Contracts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Contract-ID, 'url' => ['view', 'id' => $model->Contract-ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="main-contract-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
