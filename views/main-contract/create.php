<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MainContract */

$this->title = 'Create Main Contract';
$this->params['breadcrumbs'][] = ['label' => 'Main Contracts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-contract-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
