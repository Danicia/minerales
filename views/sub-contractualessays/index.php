<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubContractualessaysSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Contractualessays';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-contractualessays-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sub Contractualessays', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'ContractID',
            'Element',
            'Measurement',
            'Minimum',
            // 'Maximum',
            // 'ApproxValue',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
