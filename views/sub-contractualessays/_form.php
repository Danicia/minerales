<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubContractualessays */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-contractualessays-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ContractID')->textInput() ?>

    <?= $form->field($model, 'Element')->textInput() ?>

    <?= $form->field($model, 'Measurement')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Minimum')->textInput() ?>

    <?= $form->field($model, 'Maximum')->textInput() ?>

    <?= $form->field($model, 'ApproxValue')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
