<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubContractualessays */

$this->title = 'Create Sub Contractualessays';
$this->params['breadcrumbs'][] = ['label' => 'Sub Contractualessays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-contractualessays-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
