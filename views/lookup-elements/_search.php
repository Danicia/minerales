<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LookupElementsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lookup-elements-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Elements') ?>

    <?= $form->field($model, 'Description') ?>

    <?= $form->field($model, 'PayableBorder') ?>

    <?= $form->field($model, 'PayableCalcVersion') ?>

    <?php // echo $form->field($model, 'MarketPrice') ?>

    <?php // echo $form->field($model, 'Measurement') ?>

    <?php // echo $form->field($model, 'Unit') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
