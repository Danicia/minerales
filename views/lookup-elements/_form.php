<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LookupElements */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lookup-elements-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Elements')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PayableBorder')->textInput() ?>

    <?= $form->field($model, 'PayableCalcVersion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MarketPrice')->textInput() ?>

    <?= $form->field($model, 'Measurement')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Unit')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
