<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubBenefitcost */

$this->title = 'Create Sub Benefitcost';
$this->params['breadcrumbs'][] = ['label' => 'Sub Benefitcosts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-benefitcost-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
