<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubBenefitcostSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-benefitcost-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Quota-ID') ?>

    <?= $form->field($model, 'BenefitCost') ?>

    <?= $form->field($model, 'AmountWMT') ?>

    <?= $form->field($model, 'AmountDMT') ?>

    <?= $form->field($model, 'CalculateDMT') ?>

    <?php // echo $form->field($model, 'GetFromContract') ?>

    <?php // echo $form->field($model, 'NotContractLinkedCosts') ?>

    <?php // echo $form->field($model, 'ID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
