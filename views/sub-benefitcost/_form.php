<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubBenefitcost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-benefitcost-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Quota-ID')->textInput() ?>

    <?= $form->field($model, 'BenefitCost')->textInput() ?>

    <?= $form->field($model, 'AmountWMT')->textInput() ?>

    <?= $form->field($model, 'AmountDMT')->textInput() ?>

    <?= $form->field($model, 'CalculateDMT')->textInput() ?>

    <?= $form->field($model, 'GetFromContract')->textInput() ?>

    <?= $form->field($model, 'NotContractLinkedCosts')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
