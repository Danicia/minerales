<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubAssay */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-assay-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Quota-ID')->textInput() ?>

    <?= $form->field($model, 'Element')->textInput() ?>

    <?= $form->field($model, 'ProvisionalValue')->textInput() ?>

    <?= $form->field($model, 'FinalValue')->textInput() ?>

    <?= $form->field($model, 'Umpire')->textInput() ?>

    <?= $form->field($model, 'Calculation')->textInput() ?>

    <?= $form->field($model, 'Payable')->textInput() ?>

    <?= $form->field($model, 'Escalator')->textInput() ?>

    <?= $form->field($model, 'AveragePrice')->textInput() ?>

    <?= $form->field($model, 'TotalPriced')->textInput() ?>

    <?= $form->field($model, 'MaterialPrice')->textInput() ?>

    <?= $form->field($model, 'TotalUnpriced')->textInput() ?>

    <?= $form->field($model, 'Penalty')->textInput() ?>

    <?= $form->field($model, 'TotalHedged')->textInput() ?>

    <?= $form->field($model, 'AveragePriceHedged')->textInput() ?>

    <?= $form->field($model, 'TreatmentCharge')->textInput() ?>

    <?= $form->field($model, 'RefiningCharge')->textInput() ?>

    <?= $form->field($model, 'TobeHedged')->textInput() ?>

    <?= $form->field($model, 'Measurement')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TobePriced')->textInput() ?>

    <?= $form->field($model, 'Unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ContractualPayable')->textInput() ?>

    <?= $form->field($model, 'PricedQuantity')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
