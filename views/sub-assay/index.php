<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubAssaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Assays';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-assay-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sub Assay', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'Quota-ID',
            'Element',
            'ProvisionalValue',
            'FinalValue',
            // 'Umpire',
            // 'Calculation',
            // 'Payable',
            // 'Escalator',
            // 'AveragePrice',
            // 'TotalPriced',
            // 'MaterialPrice',
            // 'TotalUnpriced',
            // 'Penalty',
            // 'TotalHedged',
            // 'AveragePriceHedged',
            // 'TreatmentCharge',
            // 'RefiningCharge',
            // 'TobeHedged',
            // 'Measurement',
            // 'TobePriced',
            // 'Unit',
            // 'ContractualPayable',
            // 'PricedQuantity',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
