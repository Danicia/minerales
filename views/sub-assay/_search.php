<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubAssaySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-assay-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Quota-ID') ?>

    <?= $form->field($model, 'Element') ?>

    <?= $form->field($model, 'ProvisionalValue') ?>

    <?= $form->field($model, 'FinalValue') ?>

    <?php // echo $form->field($model, 'Umpire') ?>

    <?php // echo $form->field($model, 'Calculation') ?>

    <?php // echo $form->field($model, 'Payable') ?>

    <?php // echo $form->field($model, 'Escalator') ?>

    <?php // echo $form->field($model, 'AveragePrice') ?>

    <?php // echo $form->field($model, 'TotalPriced') ?>

    <?php // echo $form->field($model, 'MaterialPrice') ?>

    <?php // echo $form->field($model, 'TotalUnpriced') ?>

    <?php // echo $form->field($model, 'Penalty') ?>

    <?php // echo $form->field($model, 'TotalHedged') ?>

    <?php // echo $form->field($model, 'AveragePriceHedged') ?>

    <?php // echo $form->field($model, 'TreatmentCharge') ?>

    <?php // echo $form->field($model, 'RefiningCharge') ?>

    <?php // echo $form->field($model, 'TobeHedged') ?>

    <?php // echo $form->field($model, 'Measurement') ?>

    <?php // echo $form->field($model, 'TobePriced') ?>

    <?php // echo $form->field($model, 'Unit') ?>

    <?php // echo $form->field($model, 'ContractualPayable') ?>

    <?php // echo $form->field($model, 'PricedQuantity') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
