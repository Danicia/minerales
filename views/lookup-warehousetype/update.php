<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LookupWarehousetype */

$this->title = 'Update Lookup Warehousetype: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Lookup Warehousetypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lookup-warehousetype-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
