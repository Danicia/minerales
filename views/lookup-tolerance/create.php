<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LookupTolerance */

$this->title = 'Create Lookup Tolerance';
$this->params['breadcrumbs'][] = ['label' => 'Lookup Tolerances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-tolerance-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
