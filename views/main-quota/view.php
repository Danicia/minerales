<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MainQuota */

$this->title = $model->Quota-ID;
$this->params['breadcrumbs'][] = ['label' => 'Main Quotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-quota-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Quota-ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Quota-ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Quota-ID',
            'Contract-ID',
            'QuotaMonth',
            'LotNumber',
            'WMT',
            'Moisture',
            'DMT',
            'FinalWeight',
            'PayableDMT',
            'Franchise',
            'CustomerLetRef',
            'ExecutionNumber',
            'ExecutionNumberFixed',
            'FinancingBank',
            'DeliveryLocation',
            'Quality',
        ],
    ]) ?>

</div>
