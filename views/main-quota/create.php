<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MainQuota */

$this->title = 'Create Main Quota';
$this->params['breadcrumbs'][] = ['label' => 'Main Quotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-quota-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
