<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MainQuota */

$this->title = 'Update Main Quota: ' . $model->Quota-ID;
$this->params['breadcrumbs'][] = ['label' => 'Main Quotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Quota-ID, 'url' => ['view', 'id' => $model->Quota-ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="main-quota-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
