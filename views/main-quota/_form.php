<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MainQuota */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-quota-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Contract-ID')->textInput() ?>

    <?= $form->field($model, 'QuotaMonth')->textInput() ?>

    <?= $form->field($model, 'LotNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'WMT')->textInput() ?>

    <?= $form->field($model, 'Moisture')->textInput() ?>

    <?= $form->field($model, 'DMT')->textInput() ?>

    <?= $form->field($model, 'FinalWeight')->textInput() ?>

    <?= $form->field($model, 'PayableDMT')->textInput() ?>

    <?= $form->field($model, 'Franchise')->textInput() ?>

    <?= $form->field($model, 'CustomerLetRef')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ExecutionNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ExecutionNumberFixed')->textInput() ?>

    <?= $form->field($model, 'FinancingBank')->textInput() ?>

    <?= $form->field($model, 'DeliveryLocation')->textInput() ?>

    <?= $form->field($model, 'Quality')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
