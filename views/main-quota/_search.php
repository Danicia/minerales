<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MainQuotaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-quota-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Quota-ID') ?>

    <?= $form->field($model, 'Contract-ID') ?>

    <?= $form->field($model, 'QuotaMonth') ?>

    <?= $form->field($model, 'LotNumber') ?>

    <?= $form->field($model, 'WMT') ?>

    <?php // echo $form->field($model, 'Moisture') ?>

    <?php // echo $form->field($model, 'DMT') ?>

    <?php // echo $form->field($model, 'FinalWeight') ?>

    <?php // echo $form->field($model, 'PayableDMT') ?>

    <?php // echo $form->field($model, 'Franchise') ?>

    <?php // echo $form->field($model, 'CustomerLetRef') ?>

    <?php // echo $form->field($model, 'ExecutionNumber') ?>

    <?php // echo $form->field($model, 'ExecutionNumberFixed') ?>

    <?php // echo $form->field($model, 'FinancingBank') ?>

    <?php // echo $form->field($model, 'DeliveryLocation') ?>

    <?php // echo $form->field($model, 'Quality') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
