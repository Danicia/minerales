<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MainQuotaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Main Quotas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-quota-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Main Quota', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Quota-ID',
            'Contract-ID',
            'QuotaMonth',
            'LotNumber',
            'WMT',
            // 'Moisture',
            // 'DMT',
            // 'FinalWeight',
            // 'PayableDMT',
            // 'Franchise',
            // 'CustomerLetRef',
            // 'ExecutionNumber',
            // 'ExecutionNumberFixed',
            // 'FinancingBank',
            // 'DeliveryLocation',
            // 'Quality',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
