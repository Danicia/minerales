<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LookupMaterial */

$this->title = 'Create Lookup Material';
$this->params['breadcrumbs'][] = ['label' => 'Lookup Materials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-material-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
