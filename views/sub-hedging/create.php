<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubHedging */

$this->title = 'Create Sub Hedging';
$this->params['breadcrumbs'][] = ['label' => 'Sub Hedgings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-hedging-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
