<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubHedgingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-hedging-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'TradeDate') ?>

    <?= $form->field($model, 'PromptDate') ?>

    <?= $form->field($model, 'Element') ?>

    <?= $form->field($model, 'Quantity') ?>

    <?= $form->field($model, 'Price') ?>

    <?php // echo $form->field($model, 'Unit') ?>

    <?php // echo $form->field($model, 'Quota-ID') ?>

    <?php // echo $form->field($model, 'BrokerRef') ?>

    <?php // echo $form->field($model, 'ID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
