<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubHedgingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Hedgings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-hedging-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sub Hedging', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'TradeDate',
            'PromptDate',
            'Element',
            'Quantity',
            'Price',
            // 'Unit',
            // 'Quota-ID',
            // 'BrokerRef',
            // 'ID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
