<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LookupPaymenttypeopex */

$this->title = 'Update Lookup Paymenttypeopex: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Lookup Paymenttypeopexes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lookup-paymenttypeopex-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
