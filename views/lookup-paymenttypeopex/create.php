<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LookupPaymenttypeopex */

$this->title = 'Create Lookup Paymenttypeopex';
$this->params['breadcrumbs'][] = ['label' => 'Lookup Paymenttypeopexes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-paymenttypeopex-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
