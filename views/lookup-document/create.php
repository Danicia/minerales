<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LookupDocument */

$this->title = 'Create Lookup Document';
$this->params['breadcrumbs'][] = ['label' => 'Lookup Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-document-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
