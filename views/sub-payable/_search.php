<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubPayableSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-payable-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Contract-ID') ?>

    <?= $form->field($model, 'Commodity') ?>

    <?= $form->field($model, 'DeductionPriorPayable') ?>

    <?= $form->field($model, 'Payable') ?>

    <?php // echo $form->field($model, 'MinDeduction') ?>

    <?php // echo $form->field($model, 'PayableUnits') ?>

    <?php // echo $form->field($model, 'TreatmentCharge') ?>

    <?php // echo $form->field($model, 'RefiningCharge') ?>

    <?php // echo $form->field($model, 'BasePrice') ?>

    <?php // echo $form->field($model, 'UpEscalator') ?>

    <?php // echo $form->field($model, 'DownEscalator') ?>

    <?php // echo $form->field($model, 'Escalator') ?>

    <?php // echo $form->field($model, 'Measurement') ?>

    <?php // echo $form->field($model, 'RCBasis') ?>

    <?php // echo $form->field($model, 'CalculationType') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
