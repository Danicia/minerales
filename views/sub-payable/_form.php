<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubPayable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-payable-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Contract-ID')->textInput() ?>

    <?= $form->field($model, 'Commodity')->textInput() ?>

    <?= $form->field($model, 'DeductionPriorPayable')->textInput() ?>

    <?= $form->field($model, 'Payable')->textInput() ?>

    <?= $form->field($model, 'MinDeduction')->textInput() ?>

    <?= $form->field($model, 'PayableUnits')->textInput() ?>

    <?= $form->field($model, 'TreatmentCharge')->textInput() ?>

    <?= $form->field($model, 'RefiningCharge')->textInput() ?>

    <?= $form->field($model, 'BasePrice')->textInput() ?>

    <?= $form->field($model, 'UpEscalator')->textInput() ?>

    <?= $form->field($model, 'DownEscalator')->textInput() ?>

    <?= $form->field($model, 'Escalator')->textInput() ?>

    <?= $form->field($model, 'Measurement')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'RCBasis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CalculationType')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
