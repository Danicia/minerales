<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubPayable */

$this->title = 'Update Sub Payable: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Sub Payables', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sub-payable-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
