<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubPayableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Payables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-payable-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sub Payable', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'Contract-ID',
            'Commodity',
            'DeductionPriorPayable',
            'Payable',
            // 'MinDeduction',
            // 'PayableUnits',
            // 'TreatmentCharge',
            // 'RefiningCharge',
            // 'BasePrice',
            // 'UpEscalator',
            // 'DownEscalator',
            // 'Escalator',
            // 'Measurement',
            // 'RCBasis',
            // 'CalculationType',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
