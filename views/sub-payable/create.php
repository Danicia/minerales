<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubPayable */

$this->title = 'Create Sub Payable';
$this->params['breadcrumbs'][] = ['label' => 'Sub Payables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-payable-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
