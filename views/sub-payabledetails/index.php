<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub Payabledetails';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-payabledetails-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Sub Payabledetails', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'ContractID',
            'Element',
            'Payable',
            'MinContent',
            // 'MaxContent',
            // 'Measurement',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
