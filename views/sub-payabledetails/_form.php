<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubPayabledetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-payabledetails-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ContractID')->textInput() ?>

    <?= $form->field($model, 'Element')->textInput() ?>

    <?= $form->field($model, 'Payable')->textInput() ?>

    <?= $form->field($model, 'MinContent')->textInput() ?>

    <?= $form->field($model, 'MaxContent')->textInput() ?>

    <?= $form->field($model, 'Measurement')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
